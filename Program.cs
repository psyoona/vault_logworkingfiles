﻿namespace PrintRepositoryPath
{
	using System;
	using System.Collections.Generic;
	using System.IO;

	class Program
	{
		static void Main(string[] args)
		{
			List<string> resultList = new List<string>();
			string[] lines = File.ReadAllLines(@"../../Original.txt");

			foreach (string line in lines)
			{
				string[] splitTab = line.Split('\t');

				foreach(string splitTabElement in splitTab)
				{
					if(splitTabElement.Contains("$"))
					{
						string[] splitSlash = splitTabElement.Split('/');
						string splitSlashElement = "";

						for (int i = 3; i < splitSlash.Length; i++)
						{
							splitSlashElement += '/' + splitSlash[i];
						}

						resultList.Add(splitSlashElement.Replace("/4600.SP/", ""));
					}
				}
			}

			resultList.Sort();
			WriteTextFile(resultList);
		}

		static private void WriteTextFile(List<string> resultList) {
			using (StreamWriter writer = new StreamWriter("../../result.txt"))
			{
				writer.WriteLine("Work datetime:\t" + DateTime.Now.ToString("MM/dd/yyyy hh:mm tt") + "\tCount: " + resultList.Count);
				foreach (string line in resultList)
				{
					writer.WriteLine("- " + line);
				}
			}
		}
	}
}
